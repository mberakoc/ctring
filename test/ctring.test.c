#include "../lib/ctring.h"

void run_test();
int main(int argc, char const *argv[])
{
	run_test();
	return EXIT_SUCCESS;
}

// Runs all test cases chosen by the programmer.
void run_test()
{
	String string = _String("Greetings, dear User. Have a good day!");
	print_string(substring(&string, index_of(string, _String("H")), length(string)));
}