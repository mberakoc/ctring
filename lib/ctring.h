#include <stdio.h>
#include <stdlib.h>

typedef enum
{
	FALSE, TRUE
} Boolean;
typedef struct CharList CharList;
typedef CharList * CharListPtr;
typedef struct String String;
typedef String * StringPtr;

struct CharList
{
	char char_unit;
	CharListPtr next_ptr;
	CharListPtr previous_ptr;
};

struct String
{
	CharListPtr char_list_header;
	CharListPtr char_list_tail;
	unsigned length;
	StringPtr id;
};

const int INDEX_OUT_OF_BOUNDS_ERROR_CODE = 37;

void append_char_unit(CharListPtr *char_list_header_ptr, CharListPtr *char_list_tail_ptr, char char_unit);
unsigned length(String string);
String _String(char char_array[])
{
	StringPtr string = (StringPtr) malloc(sizeof(String));
	if (string != NULL)
	{
		int index = 0;
		string->char_list_header = NULL;
		string->char_list_tail = NULL;
		while (*(char_array + index) != '\0')
		{
			append_char_unit(&(string->char_list_header),
			 &(string->char_list_tail), *(char_array + index++));
		}
		string->length = length(*string);
		string->id = string;
		return *string;
	}
	else
	{
		_String(char_array);
	}
	return *string;
}

void append_char_unit(CharListPtr *char_list_header_ptr, CharListPtr *char_list_tail_ptr, char char_unit)
{
	CharListPtr new_char_list_ptr = (CharListPtr) malloc(sizeof(CharList));
	if (new_char_list_ptr != NULL)
	{
		new_char_list_ptr->char_unit = char_unit;
		new_char_list_ptr->next_ptr = NULL;
		new_char_list_ptr->previous_ptr = NULL;
		if (*char_list_header_ptr == NULL)
		{
			*char_list_header_ptr = new_char_list_ptr;
			*char_list_tail_ptr = new_char_list_ptr;
			return;
		}
		CharListPtr current_char_list_ptr = *char_list_header_ptr;
		while (current_char_list_ptr->next_ptr != NULL)
		{
			current_char_list_ptr = current_char_list_ptr->next_ptr;
		}
		*char_list_tail_ptr = new_char_list_ptr;
		new_char_list_ptr->previous_ptr = current_char_list_ptr;
		current_char_list_ptr->next_ptr = new_char_list_ptr;
	}
	else
	{
		append_char_unit(char_list_header_ptr, char_list_tail_ptr, char_unit);
	}
}

unsigned length(String string)
{
	CharListPtr current_char_list_ptr = string.char_list_header;
	unsigned length = 0;
	while (current_char_list_ptr != NULL)
	{
		current_char_list_ptr = current_char_list_ptr->next_ptr;
		++length;
	}
	return length;
}

// For string matching: https://en.wikipedia.org/wiki/String-searching_algorithm
// Plan: Implement one of them. Current one: Naive string matching
char char_at(String string, unsigned index);
int index_of(String string, String key)
{
	int index = -1;
	Boolean is_key_found;
	CharListPtr current_char_list_ptr = string.char_list_header;
	for (unsigned i = 0; i <= string.length - key.length; ++i)
	{
		for (unsigned j = 0; j < key.length; ++j)
		{
			is_key_found = TRUE;
			if (char_at(string, i + j) != char_at(key, j))
			{
				is_key_found = FALSE;
				break;
			}
		}
		if (is_key_found)
		{
			index = i;
			break;
		}
	}
	return index;
}

char char_at(String string, unsigned index)
{
	if (index >= string.length)
	{
		fprintf(stderr, "%s\n", "Error: Index out of bounds.");
		exit(INDEX_OUT_OF_BOUNDS_ERROR_CODE);
	}
	if (index < (int)(string.length / 2 + 0.5))
	{
		CharListPtr current_char_list_ptr = string.char_list_header;
		for (unsigned i = 0; i < index; ++i)
		{
			current_char_list_ptr = current_char_list_ptr->next_ptr;
		}
		return current_char_list_ptr->char_unit;
	}
	else 
	{
		CharListPtr current_char_list_ptr = string.char_list_tail;
		for (unsigned i = 0; i < string.length - index - 1; ++i)
		{
			current_char_list_ptr = current_char_list_ptr->previous_ptr;
		}
		return current_char_list_ptr->char_unit;
	}
}

void reverse(String string)
{
	CharListPtr left_char_list_ptr = string.char_list_header;
	CharListPtr right_char_list_ptr = string.char_list_tail;
	for (int i = 0; i < string.length / 2; ++i)
	{
		char temp = left_char_list_ptr->char_unit;
		left_char_list_ptr->char_unit = right_char_list_ptr->char_unit;
		right_char_list_ptr->char_unit = temp;
		left_char_list_ptr = left_char_list_ptr->next_ptr;
		right_char_list_ptr = right_char_list_ptr->previous_ptr;
	}
}

void print_string(String string)
{
	CharListPtr char_list_header = string.char_list_header;
	while (char_list_header != NULL)
	{
		printf("%c", char_list_header->char_unit);
		char_list_header = char_list_header->next_ptr;
	}
	puts("");
}

char * convert_to_char_array(String string)
{
	char * char_array = (char *) malloc(string.length + 1);
	CharListPtr current_char_list_ptr = string.char_list_header;
	int index = 0;
	while (current_char_list_ptr != NULL)
	{
		*(char_array + index++) = current_char_list_ptr->char_unit;
		current_char_list_ptr = current_char_list_ptr->next_ptr;
	}
	*(char_array + index) = '\0';
	return char_array;
}

String copy(String string)
{
	return _String(convert_to_char_array(string));
}

// Prints a string in a complex way
void __print_string(String string)
{
	int index = 0;
	CharListPtr current_ptr = string.char_list_header;
	printf("ADDR[%d]: %p\n", index++, current_ptr);
	while (current_ptr != NULL)
	{
		printf("CHAR[%d]: %c\n", index - 1, current_ptr->char_unit);
		current_ptr = current_ptr->next_ptr;
		printf("ADDR[%d]: %p\n", index++, current_ptr);
	}
}

void insert(StringPtr core_ptr, String node, int index)
{
	if (index > (*core_ptr).length || index < 0)
	{
		fprintf(stderr, "%s\n", "Error: Index out of bounds.");
		exit(INDEX_OUT_OF_BOUNDS_ERROR_CODE);
	}
	String node_mimic = copy(node);
	if (index == 0)
	{
		node_mimic.char_list_tail->next_ptr = core_ptr->char_list_header;
		core_ptr->char_list_header = node_mimic.char_list_header;
		(*core_ptr).length = length(*core_ptr);
		return;
	}
	CharListPtr current_core_ptr = (*core_ptr).char_list_header;
	for (int i = 0; i < index - 1; i++)
	{
		current_core_ptr = current_core_ptr->next_ptr;
	}
	CharListPtr linking_core_ptr = current_core_ptr->next_ptr;
	current_core_ptr->next_ptr = node_mimic.char_list_header;
	node_mimic.char_list_tail->next_ptr = linking_core_ptr;
	if (linking_core_ptr != NULL) linking_core_ptr->previous_ptr = node_mimic.char_list_tail;
	(*core_ptr).length = length(*core_ptr);
}

void append(StringPtr core_ptr, String node)
{
	insert(core_ptr, node, core_ptr->length);
}

void prepend(StringPtr core_ptr, String node)
{
	insert(core_ptr, node, 0);
}

char __MapToUpperCase(char input)
{
	char output;
	if (input < 'a' || input > 'z') return input;
	output = (char) (input - ('a' - 'A'));
	return output;
}

char __MapToLowerCase(char input)
{
	char output;
	if (input < 'A' || input > 'Z') return input;
	output = (char) (input + ('a' - 'A'));
	return output;
}

void map(String string, char (__Map)(char))
{
	CharListPtr current_char_list_ptr = string.char_list_header;
	while (current_char_list_ptr != NULL)
	{
		current_char_list_ptr->char_unit = __Map(current_char_list_ptr->char_unit);
		current_char_list_ptr = current_char_list_ptr->next_ptr;
	}
}

void shrink(StringPtr string_ptr, int begin_index, int end_index)
{
	unsigned len = length(*string_ptr);
	if (begin_index > end_index || begin_index > len || begin_index < 0 || end_index > len || end_index < 0)
	{
		fprintf(stderr, "%s\n", "Error: Index out of bounds.");
		exit(INDEX_OUT_OF_BOUNDS_ERROR_CODE);
	}
	CharListPtr forward_ptr = (*string_ptr).char_list_header;
	CharListPtr backup_ptr;
	for (int i = 0; i < begin_index; i++)
	{
		backup_ptr = forward_ptr;
		forward_ptr = forward_ptr->next_ptr;
		free(backup_ptr);
	}
	(*string_ptr).char_list_header = forward_ptr;
	CharListPtr backward_ptr = (*string_ptr).char_list_tail;
	for (int j = 0; j < len - end_index; ++j)
	{
		backup_ptr = backward_ptr;
		backward_ptr = backward_ptr->previous_ptr;
		free(backup_ptr);
	}
	(*string_ptr).char_list_tail = backward_ptr;
}

String substring(StringPtr string_ptr, int begin_index, int end_index)
{
	String replica = copy(*string_ptr);
	shrink(&replica, begin_index, end_index);
	return replica;
}