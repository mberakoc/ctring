if [[ $# != 1 ]]
then
    echo "You must enter a message for commit."
    exit
fi
message=$1
git add -A
git commit -m "$message"
git push origin master